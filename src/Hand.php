<?php

namespace Ronnie25\PokerValidator;

class Hand
{
    /**
     * Contains all the cards in the hand
     *
     * @var Card[]
     */
    protected $cards = [];

    /**
     * String containing all the ranks in the hand ordered descending\
     * Used for Straight check
     *
     * @var string
     */
    protected $orderedRanks;

    /**
     * Array with the count of each rank in the hand
     * Used for checking pairs, 3 of a kind, 4 of a kind
     *
     * @var int[]
     */
    protected $ranksCount = [];

    /**
     * Contains all the ranks in a descending order
     * Hands are checked upon this for deciding if it's a straight or not
     *
     * @var string
     */
    protected static $straightOrder;

    /**
     * Int rank of the Hand
     * [1-10]
     *
     * @var int
     */
    protected $handRank;

    /**
     * Hand constructor.
     * @param Card[] $cards
     */
    public function __construct(array $cards = [])
    {
        $this->cards = $cards;
    }

    /**
     * @return int
     */
    public function getHandRank(): int
    {
        if ($this->handRank) {
            return $this->handRank;
        }

        $this->handRank = $this->calculateRank();
        return $this->handRank;
    }

    /**
     * Adds a card to the hand
     *
     * @param Card $card
     * @throws \Exception
     */
    public function addCard(Card $card): void
    {
        if (count($this->cards) >= 5) {
            throw new \Exception('Hand is full');
        }

        if (in_array($card, $this->cards)) {
            throw new \Exception('Card ' . $card . ' already exists in hand');
        }

        $sameRankCount = 0;
        foreach ($this->cards as $cardInHand) {
            $sameRankCount += $cardInHand->getRank() == $card->getRank() ? 1 : 0;
        }

        if ($sameRankCount >= 4) {
            throw new \Exception('Hand already has 4 cards of the ' . $card->getRank() . ' rank');
        }

        $this->cards[] = $card;
    }

    /**
     *
     * @return string
     */
    protected static function getStraightOrder(): string
    {
        if (!self::$straightOrder) {
            $cardRanks = Card::getRanks();
            self::$straightOrder = implode('', $cardRanks);
        }

        return self::$straightOrder;
    }

    public function __toString(): string
    {
        return implode(' ', $this->cards);
    }

    /**
     * Checks if the hand contains a flush
     *
     * @return bool
     */
    protected function isFlush(): bool
    {
        $suite = $this->cards[0]->getSuite();
        foreach ($this->cards as $card) {
            if ($suite != $card->getSuite()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Creates an string that has all the card ranks in hand ordered in a descending manner
     *
     * @return string
     */
    protected function getOrderedRanks(): string
    {
        if (!$this->orderedRanks) {
            $ranks = [];
            foreach ($this->cards as $card) {
                $ranks[] = $card->getRank();
            }

            usort($ranks, function ($a, $b) {
                if ($a == $b) {
                    return 0;
                }

                // TODO optimize; costly operation
                return array_search($a, Card::getRanks()) < array_search($b, Card::getRanks()) ? -1 : 1;
            });
            $this->orderedRanks = implode('', $ranks);
        }

        return $this->orderedRanks;
    }

    /**
     * Checks if the hand has an Ace high straight
     * Used for checking if the hand has a Royal flush
     *
     * @return bool
     */
    protected function isAceHighStraight(): bool
    {
        return strpos(self::$straightOrder, $this->orderedRanks) === 0;
    }

    /**
     * Checks if the hand has a straight
     *
     * @return bool
     */
    protected function isStraight(): bool
    {
        return strpos(self::$straightOrder, $this->orderedRanks) !== false || $this->orderedRanks === 'A5432';
    }

    /**
     * Creates array with count of each rank in the hand
     */
    protected function calculateRanksCount(): void
    {
        if (!empty($this->ranksCount)) {
            return;
        }

        foreach ($this->cards as $card) {
            if (!isset($this->ranksCount[$card->getRank()])) {
                $this->ranksCount[$card->getRank()] = 0;
            }

            $this->ranksCount[$card->getRank()]++;
        }
    }

    /**
     * Calculates the Hand Rank
     * In which Royal Flush is 1
     * High Card is 10
     *
     * @return int
     */
    public function calculateRank(): int
    {
        $this->getOrderedRanks();
        $isFlush = $this->isFlush();

        // Royal flush
        if ($isFlush && $this->isAceHighStraight()) {
            return 1;
        }

        // Straight flush
        $this->getStraightOrder();
        $isStraight = $this->isStraight();
        if ($isFlush && $isStraight) {
            return 2;
        }

        // Four of a kind
        $this->calculateRanksCount();
        $hasFourOfAKind = false;
        foreach ($this->ranksCount as $suiteCount) {
            if ($suiteCount == 4) {
                $hasFourOfAKind = true;
                break;
            }
        }
        if ($hasFourOfAKind) {
            return 3;
        }

        // Full house
        $hasThreeOfAKind = false;
        $hasAPair = false;
        $pairCount = 0;
        foreach ($this->ranksCount as $suiteCount) {
            if ($suiteCount == 3) {
                $hasThreeOfAKind = true;
            } elseif ($suiteCount == 2) {
                $pairCount++;
                $hasAPair = true;
            }
        }
        if ($hasThreeOfAKind && $hasAPair) {
            return 4;
        }

        // Flush
        if ($isFlush) {
            return 5;
        }

        // Straight
        if ($isStraight) {
            return 6;
        }

        // Three of a kind
        if ($hasThreeOfAKind) {
            return 7;
        }

        // Two pairs
        if ($pairCount == 2) {
            return 8;
        }

        // Pair
        if ($hasAPair) {
            return 9;
        }

        // High card
        return 10;
    }
}