<?php

namespace Ronnie25\PokerValidator;

class Card
{
    protected const SUITES = ['♠', '♥', '♦', '♣'];
    protected const RANKS = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2'];

    /**
     * @var string
     */
    protected $suite;

    /**
     * @var string
     */
    protected $rank;

    /**
     * Card constructor.
     * @param string $suite
     * @param string $rank
     * @throws \Exception
     */
    public function __construct(string $suite, string $rank)
    {
        $this->validateRank($rank);
        $this->validateSuite($suite);
        $this->suite = $suite;
        $this->rank = $rank;
    }

    public static function getSuites()
    {
        return self::SUITES;
    }

    public static function getRanks()
    {
        return self::RANKS;
    }

    /**
     * @param string $suite
     * @return bool
     * @throws \Exception
     */
    protected function validateSuite(string $suite)
    {
        if (!in_array($suite, self::SUITES)) {
            throw new \Exception('Invalid suite ' . $suite);
        }
        return true;
    }

    /**
     * @param string $rank
     * @return bool
     * @throws \Exception
     */
    protected function validateRank(string $rank)
    {
        if (!in_array($rank, self::RANKS)) {
            throw new \Exception('Invalid rank ' . $rank);
        }
        return true;
    }

    /**
     * @return string
     */
    public function getSuite(): string
    {
        return $this->suite;
    }

    /**
     * @return string
     */
    public function getRank(): string
    {
        return $this->rank;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->rank . $this->suite;
    }
}