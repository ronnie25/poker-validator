<?php

namespace Ronnie25\PokerValidator;

class Set
{
    /**
     * @var Hand[]
     */
    protected $hands = [];

    /**
     *
     * @var Hand[]
     */
    protected $sortedHands = [];

    /**
     * Set constructor.
     * @param $hands
     */
    public function __construct(array $hands = [])
    {
        $this->hands = $hands;
    }

    public function addHand(Hand $hand): void
    {
        $this->hands[] = $hand;
    }

    /**
     * Sorts all the hands in a provided set
     */
    public function sort(): void
    {
        $this->sortedHands = $this->hands;
        usort($this->sortedHands, function ($handA, $handB) {
            return $handA->getHandRank() <  $handB->getHandRank() ? -1 : 1;
        });
    }

    public function __toString(): string
    {
        return implode("\n", $this->hands) . "\n";
    }

    /**
     * Prints out the sorted hands
     *
     * @return string
     */
    public function printSortedHands()
    {
        return implode("\n", $this->sortedHands) . "\n";

    }

}