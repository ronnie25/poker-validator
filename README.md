# poker-validator
Validates and sorts poker hands

###TODO
Sort equally ranked hands through high card
Exception handing


###Install as a composer library
```
composer require ronnie25/poker-validator
```

###Run test cases
```
# To run the default test in test.txt
php cli.php

# To run another test

php cli.php path/to/file.txt
```
