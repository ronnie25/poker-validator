<?php

require 'vendor/autoload.php';
use \Ronnie25\PokerValidator;

$file = $argv[1] ?? 'tests/test.txt';
if (!file_exists($file)) {
   echo 'File ' . $file . ' does not exist' . "\n";
   exit;
}

$contents = trim(file_get_contents($file));
$handsContents = explode("\n", $contents);

$set = new PokerValidator\Set();
foreach ($handsContents as $handContents) {
    $hand = new PokerValidator\Hand();
    $cardsContents = explode(' ', trim($handContents));
    foreach ($cardsContents as $cardContents) {
        $suite = mb_substr(trim($cardContents), -1);
        $rank = mb_substr(trim($cardContents), 0,-1);

        $card = new PokerValidator\Card($suite, $rank);
        $hand->addCard($card);
    }
    $set->addHand($hand);
}

echo "Set:\n";
echo $contents;
echo "\n\n";
echo "Ordered set:\n";
$set->sort();
echo $set->printSortedHands();
